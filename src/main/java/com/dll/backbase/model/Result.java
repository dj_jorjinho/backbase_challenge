package com.dll.backbase.model;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;

public class Result {

    private String formattedAddress;
    private String latitude;
    private String longitude;

    public String getFormattedAddress() {
        return formattedAddress;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setFormattedAddress(String formattedAddress) {
        this.formattedAddress = formattedAddress;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public static class ResultDeserializer extends StdDeserializer<Result> {
        public ResultDeserializer() {
            super(Result.class);
        }

        @Override
        public Result deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
            Result out = new Result();
            JsonNode node = jp.getCodec().readTree(jp);
            JsonNode result = node.get("result");
            JsonNode location = result.get("geometry").get("location");

            out.setFormattedAddress(result.get("formatted_address").asText());
            out.setLatitude(location.get("lat").asText());
            out.setLongitude(location.get("lng").asText());

            return out;
        }
    }
}
