package com.dll.backbase;

import com.dll.backbase.model.Result;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jacksonxml.JacksonXMLDataFormat;
import org.springframework.stereotype.Component;

import static java.lang.String.format;
import static org.apache.camel.Exchange.CONTENT_TYPE;
import static org.apache.camel.Exchange.HTTP_QUERY;
import static org.apache.camel.ExchangePattern.InOut;

@Component
public class GoogleGeocodeRoute extends RouteBuilder {
    private static final String API_KEY = "AIzaSyCgLkEGhkfVOhn1nHYo0LoF7wAlyJppYEQ";
    private static final String URL = "https4://maps.googleapis.com/maps/api/geocode/xml?bridgeEndpoint=true";

    @Override
    public void configure() throws Exception {
        XmlMapper mapper = new XmlMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Result.class, new Result.ResultDeserializer());
        mapper.registerModule(module);
        JacksonXMLDataFormat df = new JacksonXMLDataFormat(mapper, Result.class);

        from("direct:geocode")
            .setExchangePattern(InOut)
            .process(exchange -> exchange.getIn().setHeader(HTTP_QUERY,
                    format("address=%s&key=%s", exchange.getIn().getHeader("addr"), API_KEY)))
            .toD(URL)
            .unmarshal(df)
            .setHeader(CONTENT_TYPE, constant("application/json"))
        ;

    }

}
