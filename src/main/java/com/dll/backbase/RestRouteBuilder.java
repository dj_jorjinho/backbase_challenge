package com.dll.backbase;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

import static org.apache.camel.model.rest.RestBindingMode.json;
import static org.apache.camel.model.rest.RestParamType.query;

@Component
public class RestRouteBuilder extends RouteBuilder {

    @Override
    public void configure() throws Exception {
        restConfiguration()
            .bindingMode(json)
            .apiContextPath("/api-doc")
            .apiProperty("api.title", "User API").apiProperty("api.version", "1.0.0")
            .apiProperty("cors", "true");

        rest("/geo").description("Geocoding service")
            .produces("application/json")
            .get()
                .param().name("addr").description("Query by a known Address")
                .type(query).dataType("string").endParam()
            .responseMessage().code(200).message("Address found").endResponseMessage()
            .route()
                .inOut("direct:geocode")
                .setHeader("addr", simple("${header.addr}"))
        ;
    }

}
