package com.dll.backbase;

import com.dll.backbase.model.Result;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Import(Main.class)
public class MainIT {

    @Autowired
    private TestRestTemplate rest;

    @Test
    public void testValidAddress() {
        Result body = rest.getForEntity("/camel/geo?addr=New%20York", Result.class).getBody();
        assertThat(body.getFormattedAddress(), is("New York, NY, USA"));
        assertThat(body.getLatitude(), is("40.7127837"));
        assertThat(body.getLongitude(), is("-74.0059413"));
    }

}