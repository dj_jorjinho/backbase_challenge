# Code Challenge

Use an address to determine geolocation.

## Build

> mvn clean install

## Usage

> mvn spring-boot:run

> curl -v localhost:8080/camel/api-doc

> curl -v localhost:8080/camel/geo?addr=New%20York